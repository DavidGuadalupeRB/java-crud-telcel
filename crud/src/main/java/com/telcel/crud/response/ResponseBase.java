/**
 * TELCEL CRUD
 * Representación de la entidad MOVILES
 * Control de versiones:
 * Version  Date/Hour               	  By                 				Company     Description
 * -------  -------------------     	  ----------------    				--------    -----------------------------------------------------------------
 * 1.0      09/05/2024/04:20         	David Guadalupe Robles Barcenas     NTTDATA		Creacion de clase Response para el movil*/
package com.telcel.crud.response;

import java.io.Serializable;

import lombok.Getter;
import lombok.Setter;

/**
 * 
 * Descripcion: ResponseBase para respuesta de consumo general
 *
 * @author DavidGRB
 *
 */
@Getter
@Setter
public class ResponseBase<T> implements Serializable {
	 private static final long serialVersionUID = -5953885336384469795L;

	    private String message;
	    private transient T data;
}

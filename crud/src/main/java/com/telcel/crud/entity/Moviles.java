/**
 * TELCEL CRUD
 * Representación de la entidad MOVILES
 * Control de versiones:
 * Version  Date/Hour               	  By                 				Company     Description
 * -------  -------------------     	  ----------------    				--------    -----------------------------------------------------------------
 * 1.0      09/05/2024/04:20         	David Guadalupe Robles Barcenas     NTTDATA		Creacion de clase entity para el movil*/
package com.telcel.crud.entity;


import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.Table;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Entity
@Table(name="MOVILES")
public class Moviles {
	
    /**
     * ID de serializacion de la clase
     */
    @SuppressWarnings("unused")
	private static final long serialVersionUID = 5340097334570017414L;
    

    /**
     * Identificador del registro del movil
     */
    @Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    @Column(name="ID")
    private long idMovil;
    
    /**
     * Campo que referencia la marca del movil
     */
    @Column(name="MARCA")
    private String txtMarca;

    /**
     * Campo que referencia el modelo del movil
     */
    @Column(name="MODELO")
    private String txtModelo;
    
    /**
     * Campo que referencia el año del movil
     */
    @Column(name="ANIO")
    private String anio;
    
    /**
     * Campo que referencia el color del movil
     */
    @Column(name="COLOR")
    private String txtColor;
    
    /**
     * Campo que referencia la memoria del movil
     */
    @Column(name="MEMORIA")
    private String txtMemoria;
}

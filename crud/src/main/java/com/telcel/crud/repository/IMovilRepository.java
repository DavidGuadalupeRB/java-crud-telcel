package com.telcel.crud.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import com.telcel.crud.entity.Moviles;


public interface IMovilRepository extends JpaRepository<Moviles, Integer>{


}

/**
 * TELCEL CRUD
 * Representación de la entidad MOVILES
 * Control de versiones:
 * Version  Date/Hour               	  By                 				Company     Description
 * -------  -------------------     	  ----------------    				--------    -----------------------------------------------------------------
 * 1.0      09/05/2024/04:20         	David Guadalupe Robles Barcenas     NTTDATA		Creacion de clase Controller para el movil*/

package com.telcel.crud.controller;


import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.owasp.encoder.Encode;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.telcel.crud.constants.APPConstanst;
import com.telcel.crud.entity.Moviles;
import com.telcel.crud.repository.IMovilRepository;
import com.telcel.crud.request.MovilRequest;
import com.telcel.crud.response.ResponseBase;
import io.swagger.annotations.ApiOperation;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.validation.Valid;


/**
 * Controlador para la gestión de moviles
 * @author David Robles Barcenas
 */
@RestController
@RequestMapping("/moviles")
@CrossOrigin(origins = "http://localhost:4200")
public class MovilController {

	
	 /**
     * Logger
     */
    private static final Logger LOGGER = LoggerFactory.getLogger(MovilController.class);
    
    /**
     * Referencia hacia MovilService
     */
    @Autowired(required = false) 
    private IMovilRepository movilRepository;
    
    
    
    /**
     * Metodo POST que se encargara del alta de un nuevo movil 
     *
     * @param movilRequest objeto que se espera con los valores para el alta del movil
     * @return Codigo de la operacion y objeto JSON obtenido al insertar el movil
     * @throws TelcelServiceException Excepcion 
     */
    @ApiOperation(value = "Alta de un movil en la BD Telcel", notes = "En el body se devuelve el objeto creado", response = ResponseEntity.class, httpMethod = "POST")
    @PostMapping(consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<?> altaMovil(HttpServletRequest request,
            @Valid @RequestBody MovilRequest movilRequest) {
        LOGGER.debug("Alta de Movil Telcel [" + Encode.forJava(movilRequest.getMarca() + " " + movilRequest.getModelo()) + "]");
        HttpStatus httpStatus = HttpStatus.CREATED;

        ResponseBase<Moviles> response = new ResponseBase<Moviles>();
        try {
        	Moviles _movil = movilRepository
                .save(new Moviles());
            response.setData(_movil);
          } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
          }
        return ResponseEntity.status(httpStatus).body(response);
    }

    
    /**
     * Metodo PUT que se encargara de la actualización del movil
     * @param movilRequest objeto que se espera con los valores para actualizar del movil
     * @return Codigo de la operacion y objeto JSON obtenido al actualizar el registro
     * @throws TelcelServiceException Excepcion 
     */
    @ApiOperation(value = "Edicion de un movil", notes = "En el body se devuelve el objeto creado", response = ResponseEntity.class, httpMethod = "PUT")
    @PutMapping(consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<?> edicionUsuario(HttpServletRequest request, @PathVariable("id") long id,
            @Valid @RequestBody MovilRequest movilRequest) {
        LOGGER.debug("Entrando en edicion de movil Telcel [" + Encode.forJava(movilRequest.getMarca() + " " + movilRequest.getModelo()) + "]");
        Optional<Moviles> movilData = movilRepository.findById((int) id);

        if (movilData.isPresent()) {
        	Moviles _movil = movilData.get();
        	_movil.setTxtMarca(movilRequest.getMarca());
        	_movil.setTxtModelo(movilRequest.getModelo());
        	_movil.setAnio(movilRequest.getAnio());
        	_movil.setTxtColor(movilRequest.getColor());
        	_movil.setTxtMemoria(movilRequest.getMemoria());
        	
          return new ResponseEntity<>(movilRepository.save(_movil), HttpStatus.OK);
        } else {
          return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }
    
    
	/**
	 * Metodo GET para la obtención de los moviles Telcel
	 * @return Devuelve la propiedad del tipo ResponseEntity<ResponseBase<List<MovilBean>>>
	 */
	@ApiOperation(value = "Servicio que obtiene los moviles de Telcel", notes = "Devuelve los moviles existentes", response = ResponseEntity.class, httpMethod = "GET")
	@GetMapping(produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<ResponseBase<List<Moviles>>> getAllMoviles() {
		LOGGER.info("Entra getAllMoviles() {}");
		ResponseBase<List<Moviles>> response = new ResponseBase<>();

		try {
		      List<Moviles> moviles = new ArrayList<Moviles>();
		      movilRepository.findAll().forEach(moviles::add);
		      if (moviles.isEmpty()) {
		        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
		      }
		        response.setData(moviles);
		        response.setMessage(APPConstanst.MSJ_SUCCESSFUL);
		    } catch (Exception e) {
		      return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
		    }
	        LOGGER.info("Sale de metodo para obtener Moviles");
	        return new ResponseEntity<>(response, HttpStatus.OK);
	}
	
	/**
	 * Metodo Delete para el borrado de los moviles Telcel
     * @return Codigo de la operacion y objeto JSON obtenido al borrar el registro
	 */
	@ApiOperation(value = "Servicio que elimina los moviles de Telcel", notes = "Elimina los moviles existentes de la BD", response = ResponseEntity.class, httpMethod = "DELETE")
	@DeleteMapping(produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<HttpStatus> deleteMovil() {
		LOGGER.info("Entra a delete Moviles() {}");
		 try {
			 movilRepository.deleteAll();
		        LOGGER.info("Sale de metodo para eliminar Moviles");
		      return new ResponseEntity<>(HttpStatus.NO_CONTENT);
		    } catch (Exception e) {
		      return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
		    }
	}

}



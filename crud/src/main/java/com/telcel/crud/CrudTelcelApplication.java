/**
 * TELCEL CRUD
 * Representación de la entidad MOVILES
 * Control de versiones:
 * Version  Date/Hour               	  By                 				Company     Description
 * -------  -------------------     	  ----------------    				--------    -----------------------------------------------------------------
 * 1.0      09/05/2024/04:26         	David Guadalupe Robles Barcenas     NTTDATA		Creacion de clase principal para el Proyecto CRUD TELCEL*/
package com.telcel.crud;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CrudTelcelApplication {

	public static void main(String[] args) {
		SpringApplication.run(CrudTelcelApplication.class, args);
	}

}

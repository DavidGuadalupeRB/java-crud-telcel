/**
 * TELCEL CRUD
 * Representación de la entidad MOVILES
 * Control de versiones:
 * Version  Date/Hour               	  By                 				Company     Description
 * -------  -------------------     	  ----------------    				--------    -----------------------------------------------------------------
 * 1.0      09/05/2024/04:20         	David Guadalupe Robles Barcenas     NTTDATA		Creacion de clase Request para el movil*/
package com.telcel.crud.request;

import com.fasterxml.jackson.annotation.JsonInclude;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * Modelo del request para un movil
 * @author DavidGRB
 */

@Getter
@Setter
@NoArgsConstructor
public class MovilRequest {
	
	 /**
     * UUI
     */
    @SuppressWarnings("unused")
	private static final long serialVersionUID = 1305007254434135992L;
    
    /**
     * Id del movil
     */
    @JsonInclude(JsonInclude.Include.NON_EMPTY)
    private Long idMovil;
    
    
    /**
     * Marca del movil
     */
    @JsonInclude(JsonInclude.Include.NON_EMPTY)
    private String marca;
    
    /**
     * Modelo del movil
     */
    @JsonInclude(JsonInclude.Include.NON_EMPTY)
    private String modelo;
    
    /**
     * Año del movil
     */
    @JsonInclude(JsonInclude.Include.NON_EMPTY)
    private String anio;
    
    /**
     * Color del movil
     */
    @JsonInclude(JsonInclude.Include.NON_EMPTY)
    private String color;

    /**
     * Memoria del movil
     */
    @JsonInclude(JsonInclude.Include.NON_EMPTY)
    private String memoria;


}

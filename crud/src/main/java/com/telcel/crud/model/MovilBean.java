/**
 * TELCEL CRUD
 * Representación de la entidad MOVILES
 * Control de versiones:
 * Version  Date/Hour               	  By                 				Company     Description
 * -------  -------------------     	  ----------------    				--------    -----------------------------------------------------------------
 * 1.0      09/05/2024/04:20         	David Guadalupe Robles Barcenas     NTTDATA		Creacion de clase Bean para el movil*/

package com.telcel.crud.model;

import java.io.Serializable;
import java.util.Date;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;


/**
 * Bean para el mapeo de la entidad de Usuarios
 * 
 * @author David Robles Barcenas
 *
 */
@JsonInclude(Include.NON_EMPTY)
@ToString(callSuper = true)
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode(callSuper=false)
@Getter
@Setter
public class MovilBean extends BaseBean implements Serializable {
	
	/**
	 * 
	 * Nombre: serialVersionUID
	 * Tipo: long
	 * Descripcion: Declaracion de variable serialVersionUID del tipo long
	 */
	private static final long serialVersionUID = 704150486912880544L;
	
	/**
	 * 
	 * Nombre: id
	 * Tipo: Long
	 * Descripcion: Declaracion de variable id del tipo Long
	 */
	private Long id;
	
	/**
	 * 
	 * Nombre: marca
	 * Tipo: String
	 * Descripcion: Declaración de la variable para la marca del movil
	 */
	private String marcaMovil;
	
	/**
	 * 
	 * Nombre: modelo
	 * Tipo: String
	 * Descripcion: Declaración de la variable para la modelo del movil
	 */
	private String modeloMovil;
	
	/**
	 * 
	 * Nombre: anio
	 * Tipo: String
	 * Descripcion: Declaración de la variable para el anio del movil
	 */
	private Date anioMovil;
	
	/**
	 * 
	 * Nombre: color
	 * Tipo: String
	 * Descripcion: Declaración de la variable para el color del movil
	 */
	private String colorMovil;
	
	/**
	 * 
	 * Nombre: memoria
	 * Tipo: String
	 * Descripcion: Declaración de la variable para la memoria del movil
	 */
	private String memoriaMovil;

}

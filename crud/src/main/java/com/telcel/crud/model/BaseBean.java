/**
 * TELCEL CRUD
 * Representación de la entidad MOVILES
 * Control de versiones:
 * Version  Date/Hour               	  By                 				Company     Description
 * -------  -------------------     	  ----------------    				--------    -----------------------------------------------------------------
 * 1.0      09/05/2024/04:20         	David Guadalupe Robles Barcenas     NTTDATA		Creacion de clase base para el movil*/

package com.telcel.crud.model;

import java.io.Serializable;

import lombok.Getter;
import lombok.Setter;

/**
 * Contiene los atributos comunes de los beans
 * 
 * @author DavidGRB
 *
 */

@Getter
@Setter
public class BaseBean implements Serializable {

	/**
	 * 
	 * Nombre: serialVersionUID
	 * Tipo: long
	 * Descripcion: Declaracion de variable serialVersionUID del tipo long
	 */
	private static final long serialVersionUID = -2070867776355629148L;
	
	
	/**
	 * 
	 * Nombre: id
	 * Tipo: Long
	 * Descripcion: Declaracion de variable id del tipo Long
	 */
	private Long id;
	
}

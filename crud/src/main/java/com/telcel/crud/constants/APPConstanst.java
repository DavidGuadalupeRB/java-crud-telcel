/**
 * TELCEL CRUD
 * Representación de la entidad MOVILES
 * Control de versiones:
 * Version  Date/Hour               	  By                 				Company     Description
 * -------  -------------------     	  ----------------    				--------    -----------------------------------------------------------------
 * 1.0      09/05/2024/04:26         	David Guadalupe Robles Barcenas     NTTDATA		Creacion de clase Constantes para el movil*/
package com.telcel.crud.constants;

/**
 * Descripcion: Define las constantes del aplicativo.
 *
 * @author DavidGRB
 */
public class APPConstanst {

	
	/**Constante Sring del code OK*/
	public static final String TELCEL_200 = "OK_200";
	
	/**
	 * La constante del mensaje OK.
	 */
	public static final String MSJ_SUCCESSFUL = "Operación Exitosa";

	/**
	 * La constante del mensaje en caso de fallo.
	 */
	public static final String MSJ_FAIL = "No se pudo realizar la operación";
}
